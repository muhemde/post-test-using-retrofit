package com.example.usingretrofit.Presenter

import com.example.usingretrofit.Model.ResponsePokemonData
import com.example.usingretrofit.Network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainPresenterPokemon(private val mainPresenterPokemonBindView: MainPresenterPokemonBindView) {
    fun getDataPokemon(){
        ApiClient.instance.getData().enqueue(object : Callback<ResponsePokemonData>{
            override fun onResponse(
                call: Call<ResponsePokemonData>,
                response: Response<ResponsePokemonData>
            ) {
                if (response.isSuccessful){
                    mainPresenterPokemonBindView.onSuccess(response.body())
                }else mainPresenterPokemonBindView.onError("Error Load Data")
            }

            override fun onFailure(call: Call<ResponsePokemonData>, t: Throwable) {
                mainPresenterPokemonBindView.onError(t.localizedMessage)
            }
        })
    }
}