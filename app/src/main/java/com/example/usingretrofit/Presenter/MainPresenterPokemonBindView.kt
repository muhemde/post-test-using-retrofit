package com.example.usingretrofit.Presenter

import com.example.usingretrofit.Model.ResponsePokemonData

interface MainPresenterPokemonBindView {
    fun onSuccess(data: ResponsePokemonData?)
    fun onError(msg : String)
}