package com.example.usingretrofit.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponsePokemonData(

	@field:SerializedName("next")
	val next: String,

	@field:SerializedName("previous")
	val previous: String,

	@field:SerializedName("count")
	val count: Int,

	@field:SerializedName("results")
	val results: List<ResultsItem?>

) : Parcelable

@Parcelize
data class ResultsItem(

	@field:SerializedName("name")
	val name: String?,

	@field:SerializedName("url")
	val url: String?

) : Parcelable
