package com.example.usingretrofit.Network

import com.example.usingretrofit.Model.ResponsePokemonData
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {

    @GET("pokemon")
    fun getData(): Call<ResponsePokemonData>
}