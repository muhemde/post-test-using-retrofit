package com.example.usingretrofit.View.Main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.example.usingretrofit.Model.ResponsePokemonData
import com.example.usingretrofit.Presenter.MainPresenterPokemon
import com.example.usingretrofit.Presenter.MainPresenterPokemonBindView
import com.example.usingretrofit.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainPresenterPokemonBindView {

    private lateinit var mainPokemonAdapter: MainPokemonAdapter
    private lateinit var mainPresenterPokemon: MainPresenterPokemon

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupComponent()
    }

    private fun setupComponent() {
        mainPresenterPokemon = MainPresenterPokemon(this@MainActivity)
        mainPresenterPokemon.getDataPokemon()
    }

    override fun onSuccess(data: ResponsePokemonData?) {
        Log.d("dataResult", data?.results.toString())
        mainPokemonAdapter = MainPokemonAdapter(data?.results,this@MainActivity)
        rvPokemon.apply {
            rvPokemon.layoutManager = GridLayoutManager(this@MainActivity, 2)
            rvPokemon.setHasFixedSize(true)
            rvPokemon.adapter = mainPokemonAdapter
        }
    }

    override fun onError(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    }
}