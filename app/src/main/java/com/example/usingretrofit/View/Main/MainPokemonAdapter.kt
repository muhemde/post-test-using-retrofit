package com.example.usingretrofit.View.Main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.usingretrofit.Model.ResultsItem
import com.example.usingretrofit.R
import kotlinx.android.synthetic.main.list_pokemon_fragment.view.*

class MainPokemonAdapter(var listDataPokemon: List<ResultsItem?>?, var context: Context) : RecyclerView.Adapter<MainPokemonAdapter.ViewHolder>() {
    private lateinit var imagePokemon: ArrayList<String>
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val pokemonImage = itemView.ivPokemon
        val pokemonName = itemView.tvPokemon
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_pokemon_fragment, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var image = listDataPokemon?.get(position)
        Glide.with(context).load(getImageUrl(image?.url!!)).into(holder.pokemonImage)
        holder.pokemonName.text = listDataPokemon?.get(position)?.name
    }

    override fun getItemCount(): Int = listDataPokemon?.size!!

    fun getImageUrl(url:String): String {
        val index = url.split("/".toRegex()).dropLast(1).last()
        return "https://pokeres.bastionbot.org/images/pokemon/$index.png"
    }

}